# if (mac OS X)
DESTDIR = ../viewer
# ENDIF
QT       += core gui opengl 

TARGET = myViewer
TEMPLATE = app

macx {
  QMAKE_CXXFLAGS += -Wno-unknown-pragmas
} else {
  QMAKE_LFLAGS += -Wno-unknown-pragmas -fopenmp 
}

SOURCES +=  \
            src/main.cpp \
            src/openglwindow.cpp \
            src/glshaderwindow.cpp

HEADERS  += \
            src/openglwindow.h \
            src/glshaderwindow.h \
    src/perlinNoise.h
# trimesh library for loading objects.
# Reference/source: http://gfx.cs.princeton.edu/proj/trimesh2/
INCLUDEPATH += ../trimesh2/include/

LIBS += -L../trimesh2/lib -ltrimesh

DISTFILES += \
    textures/chain_mail_1010962.JPG \
    textures/metal_crosshatch_pattern_6190173.JPG \
    textures/seamless-wood-texture-images-crazy-gallery-montanaesgr-fqttlywh.jpg \
    textures/Textures_p1000449.jpg \
    textures/wildtextures-seamless-wood-planks.jpg \
    textures/bricks.png \
    textures/earth1.png \
    textures/earth2.png \
    textures/earth3.png \
    textures/ennis.png \
    textures/envmap.png \
    textures/grace-new.png \
    textures/pisa.png \
    textures/uffizi-large.png \
    textures/envmap.exr \
    models/Armadillo.ply \
    models/Armadillo_LR.ply \
    models/buddha50K.ply \
    models/horse.ply \
    models/lemming.ply \
    models/lemmingLowRes.ply \
    models/sphere.ply \
    models/teapot.ply \
    shaders/gpgpu_fullrt.comp \
    shaders/1_simple.frag \
    shaders/2_phong.frag \
    shaders/3_textured.frag \
    shaders/7_noiseAlone.frag \
    shaders/8_gpgpu_spherert.frag \
    shaders/gpgpu_fullrt.frag \
    shaders/h_shadowMapGeneration.frag \
    shaders/1_simple.vert \
    shaders/2_phong.vert \
    shaders/3_textured.vert \
    shaders/7_noiseAlone.vert \
    shaders/8_gpgpu_spherert.vert \
    shaders/gpgpu_fullrt.vert \
    shaders/h_shadowMapGeneration.vert


