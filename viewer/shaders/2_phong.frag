#version 410
const float PI = 3.1415926535897932384626433832795;
const float PI_2 = 1.57079632679489661923;
const float PI_4 = 0.785398163397448309616;

uniform float lightIntensity;
uniform bool blinnPhong;
uniform float shininess;
uniform float eta;
uniform sampler2D shadowMap;
uniform bool phong;
uniform bool userChoseColor;
uniform vec4 userColor;
uniform bool artistFresnel;
uniform vec4 edgetint;
uniform vec4 reflectivity;

in vec4 eyeVector;
in vec4 lightVector;
in vec4 vertColor; //We have the same color for everything? ambient, diffuse, specular?
in vec4 vertNormal;
in vec4 lightSpace;

vec4 nNormal;
vec4 h;

out vec4 fragColor;

vec4 kAmbient = vec4(0.3);
vec4 kDiffuse = vec4( 0.3);
vec4 kSpec = vec4(1.0);
//I pass everything in camera space to the fragment shader

vec4 getHalfVector(vec4 light, vec4 view) {
    return normalize(light+view);
}

//Function that introduces refraction to the mix, calculates energy distribution between reflection and refraction.
float getFresnel(float angle) {
    float anRad = angle; //Do i need to pass the angle to radians?
    float ci = sqrt(pow(eta,2) - pow(sin(anRad), 2));
    float fs = pow(abs((cos(anRad) - ci)/(cos(anRad)+ci)),2);
    float fp = pow(abs((pow(eta,2)*cos(anRad) - ci)/(pow(eta,2)*cos(anRad) + ci)),2);

    return max((fs + fp)/2, 0.0);
}

float n_min(float r){
    return (1-r)/(1+r);
}
float n_max(float r){
    return (1+sqrt(r))/(1-sqrt(r));
}
float get_n(float r,float g){
    return n_min(r)*g + (1-g)*n_max(r);
}
float get_k2(float r, float n){
    float nr = (n+1)*(n+1)*r-(n-1)*(n-1);
    return nr/(1-r);
}
float get_r(float n, float k){
    return ((n-1)*(n-1)+k*k)/((n+1)*(n+1)+k*k);
}
float get_g(float n, float k){
    float r = get_r(n,k);
    return (n_max(r)-n)/(n_max(r)-n_min(r));
}

//Artist friendly fresnel
float getArtistFresnel(float r, float g,float theta) {
        //clamp parameters
        float _r = clamp(r,0.0,0.99);
        float _g = clamp(g,0.0,0.99);
        //compute n and k
        float n = get_n(_r,_g);
        float k2 = get_k2(_r,n);

        float c = cos(theta);
        float rs_num = n*n + k2 - 2*n*c + c*c;
        float rs_den = n*n + k2 + 2*n*c + c*c;
        float rs = rs_num/rs_den;

        float rp_num = (n*n + k2)*c*c - 2*n*c + 1;
        float rp_den = (n*n + k2)*c*c + 2*n*c + 1;
        float rp = rp_num/rp_den;

        return 0.5*(rs+rp);
}

//The diffuse intensite, output color, depends on the direction of the light inciding on the point.
vec4 getDiffuse(vec4 lightDir) {
    return kDiffuse * clamp(dot(nNormal.xyz, lightDir.xyz),0.0,1.0); //Maybe in here divide the intensity by the distance. for now we don't take it into account
}


float chiGGX(float v)
{
    return v >= 0 ? 1 : 0;
}


float ggxGeometryTerm(float thet, vec4 v, vec4 n, vec4 h, float alpha)
{
    //float dotVH = clamp(dot(v.xyz,n.xyz), 0.0, 1.0);
    float chi = chiGGX( thet / clamp(dot(v.xyz,n.xyz),0.0,1.0) );
    thet = thet * thet;
    float tan2 = ( 1 - thet ) / thet;
    return ( 2) / ( 1 + sqrt( 1 + alpha * alpha * tan2 ) );
}

float ggxDistribution(vec4 n, vec4 h, float alpha)
{
    float NoH = dot(n.xyz,h.xyz);
    float alpha2 = alpha * alpha;
    float NoH2 = NoH * NoH;
    float den = NoH2 * alpha2 + (1 - NoH2);
    return (chiGGX(NoH) * alpha2) / ( PI * den * den );
}

float specPhong(vec4 refDir, vec4 camDir) {

    return pow(max(dot(camDir.xyz, refDir.xyz), 0.0), 0.25 * shininess); //We don't use the entire value of shininess to make it look like the blinnPhong and debug
}
float specBlinnPhong(vec4 lightDir, vec4 camDir) {
    //float fresnel = getFresnel(max(dot(half.xyz, lightDir.xyz),0.0));
    return pow(max(dot(nNormal.xyz, h.xyz), 0.0), shininess);
}

float specCookTorrance(vec4 lightDir, vec4 camDir) {
    //Getting necessary angles and vectors
    float angleHalfNormal = max(dot(nNormal.xyz, h.xyz), 0.0); //Thetah
    float NdotL = max(dot(nNormal.xyz,lightDir.xyz),0.0);
    float NdotV = max(dot(nNormal.xyz,camDir.xyz),0.0);
    float roughness = shininess/200.0;

    //Calculating each component
    float dist = ggxDistribution(nNormal, h, roughness);
    float G1 = ggxGeometryTerm(NdotL, lightDir, nNormal, h,roughness);
    float G2 = ggxGeometryTerm(NdotV, camDir, nNormal, h,roughness);
    float cook = (dist*G1*G2)/(4*cos(NdotL)*cos(NdotV));
    return cook;
}

void main( void )
{
    //We normalize all the directions. normal and lightdir.
    nNormal = normalize(vertNormal);

    vec4 lightDir = lightVector; //If I did lightVector - eyeVector we have the vector between the camera and the light.
    //float distance = length(lightDir); //Distance of the light to the point. Can this count as the intensity?
    lightDir = normalize(lightDir);
    vec4 camDir = normalize(- eyeVector); //I think when we do this, we get the vector from the camera to the point(because we assume the camera is on the origin of it's space)
    h = getHalfVector(lightDir, camDir); //We use it a lot of times
    float specular = 0.0;
    vec4 baseColor = vertColor;
    vec4 fresnel = vec4(1.0);
    vec4 reflectDir = normalize(reflect(-lightDir, nNormal));
    float viewAngle = 0.0;

    viewAngle = max(dot(h.xyz, lightDir.xyz),0.0);

    if(artistFresnel) fresnel = vec4(getArtistFresnel(reflectivity.x, edgetint.x, viewAngle), getArtistFresnel(reflectivity.y, edgetint.y, viewAngle), getArtistFresnel(reflectivity.z, edgetint.z, viewAngle),1.0);
    else {
        float val = getFresnel(viewAngle);
        fresnel = vec4(val, val, val,1.0);
    }

    if(blinnPhong) {
        if(shininess >= 1) { // if it's equal to zero we asume we don't have a specular component
            if(phong) specular = specPhong(reflectDir, camDir);
            else specular = specBlinnPhong(lightDir, camDir);
        }
    } else specular = specCookTorrance(lightDir, camDir);

    vec4 diffuse = getDiffuse(lightDir);
    vec4 ambient = kAmbient;

    if(userChoseColor) baseColor = userColor;

     // This is the place where there's work to be done
     fragColor = lightIntensity*baseColor*(ambient + diffuse + specular*fresnel); //Is it good that i'm computing everything for the w value also?
     fragColor.w = 1.0;
}
