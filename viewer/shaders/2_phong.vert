#version 410

//Note to self: we don't have a model matrix because we don't want to move the models. They stay in the origin.
uniform mat4 matrix; //view matrix
uniform mat4 perspective; //projection matrix
uniform mat3 normalMatrix;
uniform bool noColor;
uniform vec3 lightPosition;

// World coordinates
in vec4 vertex;
in vec4 normal;
in vec4 color;

// Camera-space coordinates
out vec4 eyeVector;
out vec4 lightVector;
out vec4 lightSpace; // placeholder for shadow mapping
out vec4 vertColor;
out vec4 vertNormal;

void computeCameraSpace() {

}

void main( void )
{
    if (noColor) vertColor = vec4(0.2, 0.6, 0.7, 1.0 );
    else vertColor = color;
    vertNormal.xyz = normalize(normalMatrix * normal.xyz);
    vertNormal.w = 0.0;

    lightVector = matrix * (vec4(lightPosition,0) - vertex);//We need to create the vector between the light pos and the vertex before passing it to camera space.
    eyeVector = (matrix * vertex); //The vector from the point to the camera

    gl_Position = perspective * matrix * vertex;
}
